import * as authAction from '../Store/actions/auth';
import * as userAction from '../Store/actions/user';

const Const = {
  authAction,
  userAction,
};

export default Const;
