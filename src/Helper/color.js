const Color = {
  background: '#F4F6FA',
  white: '#fff',
  themePurple: '#7066DC',
  gray: '#808080',
  lightPurple: '#E0DDF3',
  lightOrange: '#FDEFE6',
  Orage: '#F3AF80',
  lightGreen: '#6ac4ba',
  lightRed: '#F0DDDC',
  textRed: '#E78380',
};

export default Color;
