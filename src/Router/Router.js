import React, {PureComponent} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  WelcomeScreen,
  SigninScreen,
  NameScreen,
  SignupScreen,
  MaleFemaleScreen,
  ProfilePictureScreen,
  SelectCategoryScreen,
  CustomizeinterestScreen,
  WelcomeFinalScreen,
} from '../Screen/AuthScreens';
import {Screen} from '../Helper';
import {
  ChooseFoodScreen,
  CommunuityScreen,
  DashboardScreen,
  MoreDetailsScreen,
  NotificationScreen,
  SettingScreen,
  SideMenuScreen,
  WalkCompleteScreen,
  WalkDetailsScreen,
  WaterDetailsScreen,
  FeedbackScreen,
  EditprofileScreen,
  HelpandsupportScreen,
  AboutusScreen,
} from '../Screen/MainScreens';
import {settingScreen} from '../Screen/DrawerScreens';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default class Router extends PureComponent {
  authStack = () => {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={Screen.WelcomeScreen} component={WelcomeScreen} />
        <Stack.Screen name={Screen.SigninScreen} component={SigninScreen} />
        <Stack.Screen name={Screen.NameScreen} component={NameScreen} />
        <Stack.Screen name={Screen.SignupScreen} component={SignupScreen} />
        <Stack.Screen
          name={Screen.MaleFemaleScreen}
          component={MaleFemaleScreen}
        />
        <Stack.Screen
          name={Screen.ProfilePictureScreen}
          component={ProfilePictureScreen}
        />
        <Stack.Screen
          name={Screen.SelectCategoryScreen}
          component={SelectCategoryScreen}
        />
        <Stack.Screen
          name={Screen.CustomizeinterestScreen}
          component={CustomizeinterestScreen}
        />
        <Stack.Screen
          name={Screen.WelcomeFinalScreen}
          component={WelcomeFinalScreen}
        />
      </Stack.Navigator>
    );
  };
  mainStack = () => {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen
          name={Screen.DashboardScreen}
          component={DashboardScreen}
        />
        <Stack.Screen
          name={Screen.MoreDetailsScreen}
          component={MoreDetailsScreen}
        />
        <Stack.Screen
          name={Screen.ChooseFoodScreen}
          component={ChooseFoodScreen}
        />
        <Stack.Screen
          name={Screen.WaterDetailsScreen}
          component={WaterDetailsScreen}
        />
        <Stack.Screen
          name={Screen.WalkDetailsScreen}
          component={WalkDetailsScreen}
        />
        <Stack.Screen
          name={Screen.WalkCompleteScreen}
          component={WalkCompleteScreen}
        />

        <Stack.Screen name={Screen.SideScreen} component={this.renderDrawer} />
      </Stack.Navigator>
    );
  };
  renderDrawer = () => {
    const {isLogin} = this.props;
    if (isLogin) {
      console.log('isLogin', isLogin);
    }
    return (
      <Drawer.Navigator
        screenOptions={{headerShown: false}}
        drawerContent={props => <SideMenuScreen {...props} />}>
        <Stack.Screen name={Screen.mainStack} component={this.mainStack} />
        <Drawer.Screen
          name={Screen.DashboardScreen}
          component={DashboardScreen}
        />
        <Drawer.Screen
          name={Screen.CommunuityScreen}
          component={CommunuityScreen}
        />
        <Drawer.Screen
          name={Screen.NotificationScreen}
          component={NotificationScreen}
        />
        <Drawer.Screen name={Screen.SettingScreen} component={SettingScreen} />
        <Stack.Screen name={Screen.FeedbackScreen} component={FeedbackScreen} />
        <Stack.Screen
          name={Screen.EditprofileScreen}
          component={EditprofileScreen}
        />
        <Stack.Screen
          name={Screen.HelpandsupportScreen}
          component={HelpandsupportScreen}
        />
        <Stack.Screen name={Screen.AboutusScreen} component={AboutusScreen} />
      </Drawer.Navigator>
    );
  };
  render() {
    const {isLogin} = this.props;
    console.log('isLogin', global.isLogin);
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName={
            global.isLogin ? Screen.SideScreen : Screen.authStack
          }>
          <Stack.Screen name={Screen.authStack} component={this.authStack} />
          <Stack.Screen
            name={Screen.SideScreen}
            component={this.renderDrawer}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
