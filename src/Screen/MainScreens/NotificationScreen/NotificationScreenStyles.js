import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    padding: 15,
    backgroundColor: 'white',
    flexDirection:'row',
    alignItems:'center',
  },
  HeadersLeftImage : {
      width:'100%',
      height:'100%',
      borderRadius:50,
  },
  HeaderLeftImageView : {
      width:40,
      height:40,
      borderRadius:40/2,
      marginLeft:15,
  },
  textview: {
    marginLeft: 15,
    marginTop: 15,
  },
  text: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  line: {
    height: 1,
    backgroundColor: 'grey',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
  },
});
