import React, {PureComponent} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {styles} from './NotificationScreenStyles';
import {Images} from '../../../Helper';

const data = [
  {
    id: '58',
    post_title: 'Heavy weight',
    postimage: Images.female3,
    post_city: 'new york',
    username: 'Anna',
    notification: 'liked your post',
    time: '10:00',
  },
  {
    id: '57',
    post_title: 'Best Gym',
    postimage: Images.male1,
    post_city: 'Gujarat',
    username: 'Jerry',
    notification: 'shared your post',
    time: '12:00',
  },
  {
    id: '55',
    post_title: 'h',
    postimage: Images.male3,
    post_city: 'new york',
    username: 'Tom',
    notification: 'shared your post',
    time: '10:00',
  },
  {
    id: '54',
    post_title: 'Heavy weight',
    postimage: Images.male3,
    post_city: 'new york',
    username: 'Tom',
    notification: 'liked your post',
    time: '10:00',
  },
  {
    id: '53',
    post_title: 'Heavy weight',
    postimage: Images.male1,
    post_city: 'new york',
    username: 'Jerry',
    notification: 'shared your post',
    time: '10:00',
  },
  {
    id: '52',
    post_title: 'Heavy weight',
    postimage: Images.male4,
    post_city: 'new york',
    username: 'Dom',
    notification: 'liked your post',
    time: '10:00',
  },
  {
    id: '51',
    post_title: 'Heavy weight',
    postimage: Images.female4,
    post_city: 'new york',
    username: 'Anna',
    notification: 'liked your post',
    time: '10:00',
  },
];
export default class NotificationScreen extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        {/* <View style={styles.textview}>
          <Text style={styles.text}> Notifications </Text>
        </View> */}
        <FlatList
          data={data}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
          renderItem={({item}) => {
            return (
              <View style={styles.container}>
                <View style={styles.HeaderLeftImageView}>
                  <Image
                    style={styles.HeadersLeftImage}
                    source={item.postimage}></Image>
                </View>
                <View style={{flexDirection: 'row', marginLeft: 10}}>
                  <View>
                    <Text style={{color: '#1B6ADF', fontSize: 15}}>
                      {item.username}
                    </Text>
                    <Text style={{color: '#64676B', fontSize: 15}}>
                      {item.time}
                    </Text>
                  </View>
                  <View>
                    <Text style={{color: '#64676B'}}>{item.notification}</Text>
                  </View>
                </View>
              </View>
            );
          }}
        />
      </View>
    );
  }
}
