import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  plate: {
    height: Rs.heightPx(10),
    width: Rs.heightPx(10),
  },
  plateView: {
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioView: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginTop: 20,
  },
  radioBtm: {
    height: Rs.heightPx(3),
    width: Rs.heightPx(3),
    tintColor: Color.themePurple,
  },
  renderMain: {
    marginTop: 20,
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Rs.widthPx(92),
    alignSelf: 'center',
  },
  grayBar: {
    width: Rs.widthPx(98),
    height: Rs.heightPx(0.1),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Color.gray,
    marginTop: 10,
    marginBottom: 10,
  },
  haderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  back: {
    width: Rs.heightPx(5),
    height: Rs.heightPx(5),
  },
  btnColor: {
    backgroundColor: Color.themePurple,
    height: Rs.heightPx(5),
    width: Rs.widthPx(35),
    borderRadius: 30,
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  btnText: {
    textAlign: 'center',
    justifyContent: 'center',
    color: Color.white,
  },
  textview1:{
    alignItems: 'center',
  },
  text1: {
    fontSize:25,
  }
});
