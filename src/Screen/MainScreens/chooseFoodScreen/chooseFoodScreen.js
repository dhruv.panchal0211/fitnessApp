import React, {PureComponent} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {Images} from '../../../Helper';
import {styles} from './chooseFoodScreenStyles';
import {AuthButton} from '../../../Componant';

export default class chooseFoodScreen extends PureComponent {
  constructor() {
    super();
    this.state = {
      radio_props: ['Snack', 'Lunch', 'Dinner', 'BreakFast'],
      value: 0,
      key: '',
      snack: [
        'Milk',
        'Eggs',
        'Banana',
        'Apple',
        'Fish',
        'Chicken',
        'Pasta',
        'Cereal',
      ],
      Lunch: [
        'Chicken',
        'Pasta',
        'Cereal',
        'Milk',
        'Eggs',
        'Banana',
        'Apple',
        'Fish',
      ],
      Dinner: [
        'Eggs',
        'Banana',
        'Apple',
        'Fish',
        'Chicken',
        'Pasta',
        'Cereal',
        'Milk',
      ],
      BreakFast: [
        'Chicken',
        'Pasta',
        'Cereal',
        'Milk',
        'Eggs',
        'Banana',
        'Apple',
        'Fish',
      ],
    };
  }

  renderFood = itemData => {
    console.log('renderFood', itemData);
    return (
      <View>
        <View style={styles.rowView}>
          <Text>{itemData.item}</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({key: itemData.index});
            }}>
            {itemData.index === this.state.key ? (
              <Image source={Images.checked} style={styles.radioBtm} />
            ) : (
              <Image style={styles.radioBtm} source={Images.unchecked} />
            )}
          </TouchableOpacity>
        </View>
        <View style={styles.grayBar} />
      </View>
    );
  };
  render() {
    return (
      <View>
        <TouchableOpacity onPress={() => this.props.navigation.pop()}>
          <View style={styles.haderView}>
            <Image source={Images.back} style={styles.back} />
          </View>
        </TouchableOpacity>
        <View style={styles.plateView}>
          <Image source={Images.plate} style={styles.plate} />
        </View>
        <View style={styles.textview1}>
          <Text style={styles.text1}>Choose Food</Text>
          <Text>
            Select Your meal and your foods {'\n'} that you consume today
          </Text>
        </View>
        <View style={styles.radioView}>
          {this.state.radio_props.map((data, key) => {
            return (
              <View style={styles.radioView}>
                {this.state.value == key ? (
                  <View>
                    <TouchableOpacity>
                      <Image source={Images.checked} style={styles.radioBtm} />
                      <Text>{data}</Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    <TouchableOpacity
                      onPress={() => this.setState({value: key})}>
                      <Image
                        source={Images.unchecked}
                        style={styles.radioBtm}
                      />
                      <Text>{data}</Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            );
          })}
        </View>
        <View>
          {this.state.value == 0 && (
            <View style={styles.renderMain}>
              <FlatList
                data={this.state.snack}
                keyExtractor={(item, index) => index}
                renderItem={this.renderFood}
              />
            </View>
          )}
          {this.state.value == 1 && (
            <View style={styles.renderMain}>
              <FlatList
                data={this.state.Lunch}
                keyExtractor={(item, index) => index}
                renderItem={this.renderFood}
              />
            </View>
          )}
          {this.state.value == 2 && (
            <View style={styles.renderMain}>
              <FlatList
                data={this.state.Dinner}
                keyExtractor={(item, index) => index}
                renderItem={this.renderFood}
              />
            </View>
          )}
          {this.state.value == 3 && (
            <View style={styles.renderMain}>
              <FlatList
                data={this.state.BreakFast}
                keyExtractor={(item, index) => index}
                renderItem={this.renderFood}
              />
            </View>
          )}
        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.pop();
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Add</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
