import React, {PureComponent} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {Screen} from '../../../Helper';
import Storage from '../../../Helper/Storage';
// import {Images, Screen, Storage} from '../../Helper';
import {styles} from './SideMenuScreenStyles';

export default class SideMenuScreen extends PureComponent {
  render() {
    console.log(this.props);
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('DashboardScreen');
          }}>
          <View style={styles.rowView}>
            {/* <Image source={Images.overview} style={styles.imgView} /> */}
            <Text style={styles.text}>Home</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.hrLine} />
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('CommunuityScreen');
          }}>
          <View style={styles.rowView}>
            {/* <Image source={Images.add_cart} style={styles.imgView} /> */}
            <Text style={styles.text}>Community</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.hrLine} />
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('NotificationScreen');
          }}>
          <View style={styles.rowView}>
            {/* <Image source={Images.user} style={styles.imgView} /> */}
            <Text style={styles.text}>Notification</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.hrLine} />
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('SettingScreen');
          }}>
          <View style={styles.rowView}>
            {/* <Image source={Images.user} style={styles.imgView} /> */}
            <Text style={styles.text}>Setting</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.hrLine} />
        <View style={styles.logout}>
          <View style={styles.hrLine} />
          <View style={styles.logout}>
            <TouchableOpacity
              onPress={() => {
                Storage.logout();
                this.props.navigation.navigate(Screen.authStack);
              }}>
              <Text style={styles.text}>Log Out</Text>
              {/* <View style={styles.rowView}>
                <Image source={Images.logout} style={styles.imgView} />
              </View> */}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
