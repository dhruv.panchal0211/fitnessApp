import React, {PureComponent} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {Images} from '../../../Helper';
import {styles} from './AboutusScreenStyles';

export default class AboutusScreen extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.haderView}>
          <TouchableOpacity onPress={() => this.props.navigation.pop()}>
            <Image source={Images.back} style={styles.back} />
          </TouchableOpacity>
        </View>
        <Text style={styles.text}>
          We {'\n'}are{'\n'}developing{'\n'}this{'\n'}app{'\n'}for{'\n'}final
          {'\n'}year{'\n'}project
        </Text>
      </View>
    );
  }
}
