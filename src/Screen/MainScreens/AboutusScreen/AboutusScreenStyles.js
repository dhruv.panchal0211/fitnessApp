import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: Color.themePurple,
    textAlign: 'center',
    fontSize: 30,
  },
  container: {
    flex: 1,
  },
  haderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Color.themePurple,
  },
  back: {
    width: Rs.heightPx(5),
    height: Rs.heightPx(5),
  },
  plus: {
    width: Rs.heightPx(2),
    height: Rs.heightPx(2),
    tintColor: Color.white,
  },
  plusView: {
    height: Rs.heightPx(3),
    width: Rs.heightPx(3),
    backgroundColor: Color.themePurple,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
});
