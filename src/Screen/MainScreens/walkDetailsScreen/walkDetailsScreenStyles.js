import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  backGround: {
    backgroundColor: Color.background,
    flex: 1,
  },
  haderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    backgroundColor: Color.background,
  },
  back: {
    width: Rs.heightPx(5),
    height: Rs.heightPx(5),
    backgroundColor: Color.background,
  },
  circleBar: {
    backgroundColor: Color.background,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 30,
  },
  verticalLine: {
    height: Rs.heightPx(5),
    width: Rs.widthPx(0.2),
    backgroundColor: Color.gray,
  },
  fillText: {
    width: Rs.widthPx(100),
    backgroundColor: Color.gray,
    alignSelf: 'center',
  },
  chartStyle: {
    marginTop: 20,
  },
  perView: {
    width: Rs.widthPx(90),
    height: Rs.heightPx(7),
    alignSelf: 'center',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  mT10: {
    marginTop: 10,
  },
  grayBar: {
    width: Rs.widthPx(98),
    height: Rs.heightPx(0.1),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Color.gray,
    marginTop: 1,
    marginBottom: 10,
  },
  textView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  titleText1: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Color.themePurple,
  },
  LeftImageView: {
    width: 40,
    height: 40,
  },
  LeftImage: {
    width: '100%',
    height: '100%',
    borderRadius: 50,
  },
});
