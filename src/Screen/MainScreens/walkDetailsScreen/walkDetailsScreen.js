import React, {PureComponent} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import {styles} from './walkDetailsScreenStyles';
import {Images, Color, Rs} from '../../../Helper';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {LineChart} from 'react-native-chart-kit';
import GoogleFit, {BucketUnit, Scopes} from 'react-native-google-fit';

const opt = {
  startDate: '2017-01-01T00:00:17.971Z', // required ISO8601Timestamp
  endDate: new Date().toISOString(), // required ISO8601Timestamp
};

export default class walkDetailsScreen extends PureComponent {
  constructor() {
    super();
    this.state = {
      dailySteps: '',
      fill: 70,
      data: {
        datasets: [
          {
            data: [0, -15.5, 46.5, 15.5, 31, 62, 15.5, 77.5, 31],
          },
        ],
        legend: ['Statistics'], // optional
      },
      xAxisLabel: [0, -15.5, 46.5, 15.5, 31, 62, 15.5, 77.5, 31],
      chartConfig: {
        backgroundGradientFrom: Color.background,
        backgroundGradientTo: Color.background,
        color: (opacity = 0) => `rgba(242, 172, 62, ${opacity})`,
        strokeWidth: 2, // optional, default 3
      },
    };
  }
  componentDidMount() {
    GoogleFit.authorize({scopes: [Scopes.FITNESS_ACTIVITY_READ]})
      .then(authResult => {
        // this.setState({status: JSON.stringify(authResult, ' ', 4)});
        GoogleFit.getDailyStepCountSamples(opt)
          .then(res => {
            console.log('Daily steps >>> ', res);
          })
          .catch(err => {
            console.warn(err);
          });
        console.log('authresult', authResult);
      })
      .catch(error => {
        console.log(error);
        // this.setState({status: 'AUTH_ERROR'});
      });
  }

  render() {
    return (
      <View style={styles.backGround}>
        <ScrollView>
          <View>
            <View>
              <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                <View style={styles.haderView}>
                  <Image source={Images.back} style={styles.back} />
                </View>
              </TouchableOpacity>
              <View style={styles.textView}>
                <Text style={styles.titleText}>
                  {' '}
                  You Walked <Text style={styles.titleText1}>
                    7000 steps
                  </Text>{' '}
                  {'\n'} today
                </Text>
              </View>
            </View>
            <AnimatedCircularProgress
              size={120}
              width={5}
              tintColor={Color.themePurple}
              backgroundColor={Color.white}
              onAnimationComplete={() => console.log('onAnimationComplete')}
              rotation={0}
              duration={3000}
              style={styles.circleBar}
              fill={this.state.fill}
            />
            <View>
              <View style={styles.rowView}>
                <View>
                  <Text>1300</Text>
                  <Text>Cal burned</Text>
                </View>
                <View style={styles.verticalLine} />
                <View>
                  <Text>8 glasses</Text>
                  <Text>Daily Glass</Text>
                </View>
              </View>
            </View>
            <LineChart
              data={this.state.data}
              width={Rs.widthPx(100)}
              height={256}
              chartConfig={this.state.chartConfig}
              bezier
              withDots={false}
              style={styles.chartStyle}
            />
            <View>
              <View style={styles.mT10}>
                <View style={styles.perView}>
                  <View style={styles.LeftImageView}>
                    <Image
                      style={styles.LeftImage}
                      source={Images.smile}></Image>
                  </View>
                  <View>
                    <Text>Best Performance</Text>
                    <Text>Monday</Text>
                  </View>
                  <View>
                    <Text>10</Text>
                  </View>
                </View>
                <View style={styles.grayBar} />
              </View>
              <View style={styles.mT10}>
                <View style={styles.perView}>
                  <View style={styles.LeftImageView}>
                    <Image style={styles.LeftImage} source={Images.sad}></Image>
                  </View>
                  <View>
                    <Text>Worst Performance</Text>
                    <Text>Sunday</Text>
                  </View>
                  <View>
                    <Text>6</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
