/* eslint-disable no-lone-blocks */
import React, {PureComponent} from 'react';
import {Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import {Images} from '../../../Helper';
import {styles} from './waterDetailsScreenStyles';

export default class waterDetailsScreen extends PureComponent {
  constructor() {
    super();
    this.state = {
      selected0: true,
      selected1: false,
      selected2: false,
      selected3: false,
      selected4: false,
      selected5: false,
      selected6: false,
      selected7: false,
      count: 1,
    };
  }

  render() {
    const {
      selected0,
      selected1,
      selected2,
      selected3,
      selected4,
      selected5,
      selected6,
      selected7,
    } = this.state;

    return (
      <View style={styles.backGround}>
        <TouchableOpacity onPress={() => this.props.navigation.pop()}>
          <View style={styles.haderView}>
            <Image source={Images.back} style={styles.back} />
          </View>
        </TouchableOpacity>
        <View style={styles.textView}>
          <Text style={styles.titleText}>
            {' '}
            You have to drink min
            <Text style={styles.titleText1}> 8 glasses</Text> {'\n'} today
          </Text>
        </View>
        <View style={styles.rowView}>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected0: !selected0})}>
              {selected0 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected1: !selected1})}>
              {selected1 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected2: !selected2})}>
              {selected2 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected3: !selected3})}>
              {selected3 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.rowView}>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected4: !selected4})}>
              {selected4 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected5: !selected5})}>
              {selected5 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected6: !selected6})}>
              {selected6 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.setState({selected7: !selected7})}>
              {selected7 === true ? (
                <Image source={Images.waterGlass} style={styles.waterGlass} />
              ) : (
                <Image source={Images.addWater} style={styles.waterGlass} />
              )}
            </TouchableOpacity>
          </View>
        </View>

        <View>
          <View style={styles.rowView}>
            <View>
              <Text>250 ml</Text>
              <Text>Water Drink</Text>
            </View>
            <View style={styles.verticalLine} />
            <View>
              <Text>8 glasses</Text>
              <Text>Daily Glass</Text>
            </View>
          </View>
        </View>
        {/* <View>
          <View style={styles.redView}>
            <Text style={styles.redText}>
              You didn't drink enough water today.
            </Text>
          </View>
        </View> */}
        <View>
          <View style={styles.mT10}>
            <View style={styles.perView}>
              <View style={styles.LeftImageView}>
                <Image style={styles.LeftImage} source={Images.smile}></Image>
              </View>
              <View>
                <Text style={{justifyContent: 'center'}}>Best Performance</Text>
                {/* <Text>Monday</Text> */}
              </View>
              <View>
                <Text>8</Text>
              </View>
            </View>
            <View style={styles.grayBar} />
          </View>
          <View style={styles.mT10}>
            <View style={styles.perView}>
              <View style={styles.LeftImageView}>
                <Image style={styles.LeftImage} source={Images.sad}></Image>
              </View>
              <View>
                <Text style={{justifyContent: 'center'}}>
                  Worst Performance
                </Text>
                {/* <Text>Sunday</Text> */}
              </View>
              <View>
                <Text>4</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
