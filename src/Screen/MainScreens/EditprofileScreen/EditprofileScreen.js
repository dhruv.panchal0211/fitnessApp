import React, {PureComponent} from 'react';
import {Image, Text, View} from 'react-native';
import {styles} from './EditprofileScreenStyles';
import {Images} from '../../../Helper';

export default class EditprofileScreen extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image style={styles.avatar} source={Images.male1} />
            <Text style={styles.name}> Dhruv </Text>
            <Text style={styles.userInfo}>dhruv.panchal0211@gmail.com</Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.item}>
            <View style={styles.iconContent}>
              <Image style={styles.icon} source={Images.home} />
            </View>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Home</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.iconContent}>
              <Image style={styles.icon} source={Images.setting} />
            </View>
            <View style={styles.infoContent}>
              <Text style={styles.info}>settings</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.iconContent}>
              <Image style={styles.icon} source={Images.notification} />
            </View>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Notification</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
