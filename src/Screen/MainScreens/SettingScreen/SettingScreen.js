import React, {PureComponent} from 'react';
import {Image, Text, View, Switch, TouchableOpacity} from 'react-native';
import {styles} from './SettingScreenStyles';
import {Images, Screen} from '../../../Helper';
import {DrawerActions} from '@react-navigation/routers';
import Storage from '../../../Helper/Storage';
export default class SettingScreen extends PureComponent {
  state = {
    toggled: false,
  };
  toggleSwitch = value => {
    this.setState({toggled: value});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.main}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.openDrawer())
            }>
            <Image source={Images.menu} style={styles.photo} />
          </TouchableOpacity>
        </View>
        <View style={styles.textview}>
          <Text style={styles.text}> Settings </Text>
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('EditprofileScreen');
            }}>
            <View style={styles.smalltextview}>
              <Text style={styles.smalltext}>Edit Profile</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.smalltextview}>
          <Text style={styles.smalltext}>Invite Friends</Text>
        </View>
        <View>
          <Text style={styles.line} />
        </View>

        <View style={styles.switchview}>
          <View style={styles.smalltextview}>
            <Text style={styles.smalltext}>Push Notification</Text>
          </View>
          <View style={styles.switchview1}>
            <Switch
              //trackColor={{false: '#767577', true: '#81b0ff'}}
              //thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}

              //onValueChange={toggleSwitch}
              //value={isEnabled}
              onValueChange={this.toggleSwitch}
              value={this.state.toggled}
            />
          </View>
        </View>

        <View>
          <Text style={styles.line} />
        </View>

        <View style={styles.switchview}>
          <View style={styles.smalltextview}>
            <Text style={styles.smalltext}>Subscribe Email</Text>
          </View>
          <View style={styles.switchview1}>
            <Switch
              //trackColor={{false: '#767577', true: '#81b0ff'}}
              //thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}

              //onValueChange={toggleSwitch}
              //value={isEnabled}
              onValueChange={this.toggleSwitch}
              value={this.state.toggled}
            />
          </View>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('FeedbackScreen');
            }}>
            <View style={styles.smalltextview}>
              <Text style={styles.smalltext}>Give Feedback</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('HelpandsupportScreen');
            }}>
            <View style={styles.smalltextview}>
              <Text style={styles.smalltext}>Help And Support</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.smalltextview}>
          <Text style={styles.smalltext}>Connect Device</Text>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.container1}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('AboutusScreen');
            }}>
            <View style={styles.smalltextview}>
              <Text style={styles.smalltext}>About Us</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
        <View style={styles.smalltextview}>
          <TouchableOpacity
            onPress={() => {
              Storage.logout();
              this.props.navigation.navigate(Screen.authStack);
            }}>
            <Text style={styles.smalltext}>Log Out</Text>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.line} />
        </View>
      </View>
    );
  }
}
