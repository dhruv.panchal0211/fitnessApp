import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.background,
  },
  main: {
    marginTop: 70,
    alignSelf: 'center',
    width: Rs.widthPx(90),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  textview: {
    marginLeft: 15,
    marginTop: 15,
  },
  text: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  photo: {
    height: Rs.heightPx(5),
    width: Rs.heightPx(5),
  },
  smalltextview: {
    marginLeft: 30,
    marginTop: 15,
  },
  smalltext: {
    fontSize: 17,
  },
  line: {
    height: 1,
    backgroundColor: 'grey',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
  },
  switchview: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginRight: 10,
  },
  switchview1: {
    marginRight: 25,
    marginTop: 14,
  },
});
