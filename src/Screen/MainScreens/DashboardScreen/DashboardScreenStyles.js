import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.background,
  },
  main: {
    height: Rs.heightPx(20),
    alignSelf: 'center',
    width: Rs.widthPx(90),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  photo: {
    height: Rs.heightPx(5),
    width: Rs.heightPx(5),
  },
  textView: {
    width: Rs.widthPx(80),
    alignSelf: 'center',
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  smallText: {
    color: Color.gray,
    fontSize: 15,
    marginTop: 25,
    lineHeight: 25,
  },
  moreDetails: {
    color: Color.themePurple,
    fontWeight: 'bold',
    marginTop: 25,
    marginBottom: 30,
  },
  whiteView: {
    backgroundColor: Color.white,
    height: Rs.heightPx(12),
    width: Rs.widthPx(98),
    alignSelf: 'center',
    elevation: 10,
    marginTop: 2,
    borderRadius: 10,
    flexDirection: 'row',
  },
  pic: {
    height: Rs.heightPx(5),
    width: Rs.heightPx(5),
  },
  picView: {
    width: Rs.widthPx(18),
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Rs.widthPx(75),
    marginTop: 15,
  },
  bold: {
    fontWeight: 'bold',
  },
  onView: {
    width: Rs.widthPx(15),
    height: Rs.heightPx(3),
    backgroundColor: Color.lightPurple,
    alignItems: 'center',
    borderRadius: 20,
  },
  warningView: {
    backgroundColor: '#FDEFE6',
    width: Rs.widthPx(20),
    height: Rs.heightPx(3),
    alignItems: 'center',
    borderRadius: 20,
  },
  onText: {
    color: Color.themePurple,
  },
  warningText: {
    color: Color.Orage,
  },
  sText: {
    color: Color.gray,
  },
});
