import {DrawerActions} from '@react-navigation/routers';
import React, {PureComponent} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TouchableNativeFeedback,
} from 'react-native';
import {Images} from '../../../Helper';
import {styles} from './DashboardScreenStyles';

export default class DashboardScreen extends PureComponent {
  render() {
    const date = new Date();
    const hour = date.getHours();
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.main}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.openDrawer())
            }>
            <Image source={Images.menu} style={styles.photo} />
          </TouchableOpacity>
          <Image source={Images.male1} style={styles.photo} />
        </View>
        <View style={styles.textView}>
          {hour < 12 && <Text style={styles.text}> Good Morning </Text>}
          {hour < 17 && hour > 12 && (
            <Text style={styles.text}> Good afternoon </Text>
          )}
          {hour < 20 && hour > 17 && (
            <Text style={styles.text}> Good evning </Text>
          )}
          {hour < 24 && hour > 20 && (
            <Text style={styles.text}> Good night </Text>
          )}
          <Text style={styles.smallText}>
            Eat the right amount of food and stay {'\n'}hydrated through the day
          </Text>
          <Text style={styles.moreDetails}>More Details</Text>
          {/* <TouchableOpacity
            onPress={() => navigation.navigate('MoreDetailsScreen')}>
          </TouchableOpacity> */}
        </View>
        <TouchableNativeFeedback
          onPress={() => navigation.navigate('MoreDetailsScreen')}>
          <View style={styles.whiteView}>
            <View style={styles.picView}>
              <Image source={Images.nutririon} style={styles.pic} />
            </View>
            <View style={styles.rowView}>
              <View>
                <Text style={styles.bold}>Nutrition</Text>
                <Text style={styles.sText}>850 cal / 1200 cal</Text>
              </View>
              <View style={styles.onView}>
                <Text style={styles.onText}>On</Text>
              </View>
            </View>
          </View>
        </TouchableNativeFeedback>

        <TouchableNativeFeedback
          onPress={() => navigation.navigate('WaterDetailsScreen')}>
          <View style={styles.whiteView}>
            <View style={styles.picView}>
              <Image source={Images.water} style={styles.pic} />
            </View>
            <View style={styles.rowView}>
              <View>
                <Text style={styles.bold}>Water</Text>
                <Text style={styles.sText}>3 / 8 glasses</Text>
              </View>
              <View style={styles.warningView}>
                <Text style={styles.warningText}>warning</Text>
              </View>
            </View>
          </View>
        </TouchableNativeFeedback>

        <TouchableNativeFeedback
          onPress={() => navigation.navigate('WalkDetailsScreen')}>
          <View style={styles.whiteView}>
            <View style={styles.picView}>
              <Image source={Images.walking} style={styles.pic} />
            </View>
            <View style={styles.rowView}>
              <View>
                <Text style={styles.bold}>Daily Step</Text>
                <Text style={styles.sText}>7000 steps / 10000 steps</Text>
              </View>
              <View style={styles.onView}>
                <Text style={styles.onText}>On</Text>
              </View>
            </View>
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}
