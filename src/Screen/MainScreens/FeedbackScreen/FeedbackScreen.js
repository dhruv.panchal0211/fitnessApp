import React, {PureComponent} from 'react';
import {Text, View, Slider, Image} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import {Images} from '../../../Helper';
import {styles} from './FeedbackScreenStyles';
import * as scale from 'd3-scale';
const size = 50;
const size75 = (size * 75) / 100;
const size50 = (size * 50) / 100;
const d3 = {
  scale,
};
var points = [
  [1, 1],
  [25, 10],
];

var multiLine = d3.scale
  .scaleLinear()
  .domain(
    points.map(function (p) {
      return p[0];
    }),
  )
  .range(
    points.map(function (p) {
      return p[1];
    }),
  );

export default class FeedbackScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      val: 1,
      smile: 1,
    };
  }
  slidingChange(val) {
    this.setState({val});
  }
  getVal(val) {
    this.setState({smile: val});
  }
  render() {
    const val = this.state.smile;
    const dVal = 'M6 10 Q19 ' + val + ' 32 10';
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Give Rating between 1 - 10!</Text>
        <Image style={{width: 100, height: 100}} source={Images.noFace}></Image>
        <Svg
          height={size75}
          width={size75}
          style={{alignSelf: 'center', marginTop: -35}}>
          <Path d={dVal} fill="none" stroke="red" strokeWidth="3" />
        </Svg>

        <Text style={styles.rating}>{parseInt(multiLine(this.state.val))}</Text>
        <Slider
          style={{width: 300}}
          step={1}
          minimumValue={1}
          maximumValue={25}
          value={this.state.val}
          onValueChange={val => this.slidingChange(val)}
          onSlidingComplete={val => this.getVal(val)}
        />
      </View>
    );
  }
}
