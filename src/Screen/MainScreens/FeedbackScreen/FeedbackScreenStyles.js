import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  LabelStyles: {
    fontSize: 14,
    fontWeight: '400',
  },
  radioView: {
    paddingHorizontal: 15,
  },
  inputView: {
    width: '100%',
    paddingTop: 20,
    height: '100%',
    backgroundColor: 'white',
  },
  inputStyle: {
    height: 100,
 
  },
  textStyle: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  textview: {
    marginLeft: 15,
    marginTop: 15,
  },
  text: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  rating: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginTop: 10,
    fontWeight: "bold"
  },
});
