import React, {PureComponent} from 'react';
import {Image, Text, View, TextInput, Button} from 'react-native';
import {styles} from './HelpandsupportScreenStyles';
import Images from '../../../Helper/Images';

export default class HelpandsupportScreen extends PureComponent {
  render() {
    return (
      <View style={styles.inputView}>
        <View style={{alignItems: 'center', paddingVertical: 40}}>
          <Image
            style={{height: 140, width: 140}}
            source={Images.contact}></Image>
        </View>
        <TextInput
          style={styles.input}
          placeholder="Subject"
          keyboardType="default"
        />
        <TextInput
          style={styles.input}
          placeholder="Your Message"
          keyboardType="default"
        />
        <Button title="send"></Button>
      </View>
    );
  }
}
