import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  haderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  back: {
    width: Rs.heightPx(5),
    height: Rs.heightPx(5),
  },
  plus: {
    width: Rs.heightPx(2),
    height: Rs.heightPx(2),
    tintColor: Color.white,
  },
  plusView: {
    height: Rs.heightPx(3),
    width: Rs.heightPx(3),
    backgroundColor: Color.themePurple,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
  textView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  oSquare: {
    width: Rs.heightPx(2),
    height: Rs.heightPx(2),
    backgroundColor: Color.Orage,
    borderRadius: 4,
  },
  rowStyles: {
    flexDirection: 'row',
    height: Rs.heightPx(4),
    alignContent: 'center',
    alignItems: 'center',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Rs.widthPx(90),
  },
  pSquare: {
    width: Rs.heightPx(2),
    height: Rs.heightPx(2),
    backgroundColor: Color.themePurple,
    borderRadius: 4,
  },
  gSquare: {
    width: Rs.heightPx(2),
    height: Rs.heightPx(2),
    backgroundColor: Color.lightGreen,
    borderRadius: 4,
  },
  grayBar: {
    width: Rs.widthPx(98),
    height: Rs.heightPx(0.1),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Color.gray,
    marginTop: 10,
    marginBottom: 10,
  },
  maintextView: {
    marginTop: 20,
  },
  squareView: {
    width: Rs.widthPx(8),
    alignItems: 'center',
  },
  whiteSquare: {
    height: Rs.heightPx(23),
    width: Rs.widthPx(100),
    marginTop: 20,
    backgroundColor: Color.white,
  },
  close: {
    height: Rs.heightPx(2),
    width: Rs.heightPx(2),
    tintColor: Color.gray,
  },
  textRow1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Rs.widthPx(90),
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 10,
  },
  titleText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  titleText1: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Color.themePurple,
  },
  grayText: {
    color: Color.gray,
  },
});
