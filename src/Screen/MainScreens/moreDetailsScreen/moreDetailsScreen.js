import React, {PureComponent} from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {Images, Rs, Color} from '../../../Helper';
import {styles} from './moreDetailsScreenStyles';
import {ProgressChart} from 'react-native-chart-kit';
import {TouchableOpacity} from 'react-native';

export default class moreDetailsScreen extends PureComponent {
  constructor() {
    super();
    this.state = {
      data: {
        labels: ['Protein', 'Crab', 'Fat'], // optional
        data: [0.63, 0.4, 0.27],
      },
      chartConfig: {
        backgroundColor: Color.gray,
        backgroundGradientFrom: Color.background,
        backgroundGradientTo: Color.background,
        color: (opacity = 1) => `rgba(112, 102, 220, ${opacity})`,
        labelColor: (opacity = 1) => Color.themePurple,
      },
    };
  }
  render() {
    return (
      <View>
        <ScrollView>
          <View style={styles.haderView}>
            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
              <Image source={Images.back} style={styles.back} />
            </TouchableOpacity>
            <View style={styles.plusView}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('ChooseFoodScreen')
                }>
                <Image source={Images.plus} style={styles.plus} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.textView}>
            <Text style={styles.titleText}>
              You burned <Text style={styles.titleText1}>850</Text> calories{' '}
              {'\n'} today
            </Text>
          </View>
          <View>
            <ProgressChart
              data={this.state.data}
              width={Rs.widthPx(100)}
              height={Rs.heightPx(30)}
              radius={40}
              strokeWidth={13}
              chartConfig={this.state.chartConfig}
              hideLegend={false}
            />
          </View>
          <View style={styles.maintextView}>
            <View style={styles.rowStyles}>
              <View style={styles.squareView}>
                <View style={styles.oSquare} />
              </View>
              <View style={styles.textRow}>
                <Text> {this.state.data.labels[0]} </Text>
                <Text>100g</Text>
                <Text> {this.state.data.data[0] * 100}% </Text>
              </View>
            </View>
            <View style={styles.grayBar} />
            <View style={styles.rowStyles}>
              <View style={styles.squareView}>
                <View style={styles.pSquare} />
              </View>
              <View style={styles.textRow}>
                <Text> {this.state.data.labels[1]} </Text>
                <Text>60g</Text>
                <Text>{this.state.data.data[1] * 100}%</Text>
              </View>
            </View>
            <View style={styles.grayBar} />

            <View style={styles.rowStyles}>
              <View style={styles.squareView}>
                <View style={styles.gSquare} />
              </View>
              <View style={styles.textRow}>
                <Text> {this.state.data.labels[2]} </Text>
                <Text>20g</Text>
                <Text>{this.state.data.data[2] * 100}%</Text>
              </View>
            </View>
          </View>
          <View style={styles.whiteSquare}>
            <View style={styles.textRow1}>
              <Text style={styles.titleText}>BreackFast</Text>
              <Image source={Images.close} style={styles.close} />
            </View>
            <View style={styles.textRow1}>
              <View>
                <Text>Eggs</Text>
                <Text style={styles.grayText}>200 Grams</Text>
              </View>
              <View>
                <Text>300</Text>
              </View>
            </View>
            <View style={styles.grayBar}></View>
            <View style={styles.textRow1}>
              <View>
                <Text>Milk</Text>
                <Text style={styles.grayText}>200 Liters</Text>
              </View>
              <View>
                <Text>100</Text>
              </View>
            </View>
          </View>

          <View style={styles.whiteSquare}>
            <View style={styles.textRow1}>
              <Text style={styles.titleText}>Lunch</Text>
              <Image source={Images.close} style={styles.close} />
            </View>
            <View style={styles.textRow1}>
              <View>
                <Text>eggs</Text>
                <Text style={styles.grayText}>200 Grams</Text>
              </View>
              <View>
                <Text>300</Text>
              </View>
            </View>
            <View style={styles.grayBar}></View>
            <View style={styles.textRow1}>
              <View>
                <Text>Milk</Text>
                <Text style={styles.grayText}>200 Liters</Text>
              </View>
              <View>
                <Text>100</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
