import WelcomeScreen from './welcomeScreen/welcomeScreen';
import SigninScreen from './SignInScreen/signinScreen';
import NameScreen from './SignupScreen/NameScreen';
import SignupScreen from './SignupScreen/SignupScreen';
import MaleFemaleScreen from './maleFemaleScreen/maleFemaleScreen';
import ProfilePictureScreen from './ProfilePictureScreen/ProfilePictureScreen';
import SelectCategoryScreen from './selectCategoryScreen/selectCategoryScreen';
import CustomizeinterestScreen from './customizeInterestScreen/customizeinterestScreen';
import WelcomeFinalScreen from './welcomeFinal/welcomeFinalScreen';

export {
  WelcomeScreen,
  SigninScreen,
  NameScreen,
  SignupScreen,
  MaleFemaleScreen,
  ProfilePictureScreen,
  SelectCategoryScreen,
  CustomizeinterestScreen,
  WelcomeFinalScreen,
};
