import React, {PureComponent} from 'react';
import {FlatList, Image, Text, View, TouchableOpacity} from 'react-native';
import {AuthButton, AuthHeader} from '../../../Componant';
import {styles} from './customizeinterestScreenStyles';
import {Images, Screen} from '../../../Helper';
import Utility from '../../../Helper/Utility';

// const Data = [Images.fashion, Images.organic];
const Data = [
  {id: 0, image: Images.fashion, text: 'Fashion'},
  {id: 1, image: Images.organic, text: 'Organic'},
  {id: 2, image: Images.meditation, text: 'Meditation'},
  {id: 3, image: Images.fitness, text: 'Fitness'},
  {id: 4, image: Images.smokeFree, text: 'Smoke-Free'},
  {id: 5, image: Images.sleep, text: 'Sleep'},
  {id: 6, image: Images.health, text: 'health'},
  {id: 7, image: Images.running, text: 'Running'},
  {id: 8, image: Images.vegan, text: 'Vegan'},
];
export default class customizeinterestScreen extends PureComponent {
  constructor() {
    super();
    this.state = {
      key: '',
    };
  }

  onPressLogin = () => {
    if (this.state.key === '') {
      Utility.showToast('Please Select At Least One Option');
      return;
    }

    this.props.navigation.navigate(Screen.WelcomeFinalScreen);
  };

  renderIntrest = itemData => {
    return (
      <View style={styles.renderMain}>
        <TouchableOpacity
          onPress={() => {
            this.setState({key: itemData.index});
          }}>
          <View
            style={
              itemData.index === this.state.key
                ? styles.selectedImageView
                : styles.imageView
            }>
            <Image source={itemData.item.image} style={styles.images} />
          </View>
          <Text style={styles.text}>{itemData.item.text}</Text>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    return (
      <View>
        <AuthHeader isBack {...this.props} />
        <Text style={styles.customizeText}>
          Time To Customize Your {'\n'} Interests
        </Text>
        <FlatList
          data={Data}
          keyExtractor={(item, index) => item.id}
          renderItem={this.renderIntrest}
          numColumns={3}
        />
        <TouchableOpacity
          onPress={() => {
            this.onPressLogin();
            // this.props.navigation.navigate(Screen.SideScreen);
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
