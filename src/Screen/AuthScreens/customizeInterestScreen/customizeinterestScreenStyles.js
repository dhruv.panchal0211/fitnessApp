import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  customizeText: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20,
  },
  renderMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  images: {
    height: Rs.heightPx(7),
    width: Rs.heightPx(7),
  },
  imageView: {
    backgroundColor: Color.white,
    height: Rs.heightPx(10),
    width: Rs.heightPx(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 20,
  },
  selectedImageView: {
    backgroundColor: Color.themePurple,
    height: Rs.heightPx(10),
    width: Rs.heightPx(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 20,
  },
  text: {
    textAlign: 'center',
  },

  btnColor: {
    backgroundColor: Color.themePurple,
    height: Rs.heightPx(5),
    width: Rs.widthPx(35),
    borderRadius: 30,
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  btnText: {
    textAlign: 'center',
    justifyContent: 'center',
    color: Color.white,
  },
});
