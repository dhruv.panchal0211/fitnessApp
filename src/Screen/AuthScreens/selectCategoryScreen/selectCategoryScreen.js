import React, {PureComponent} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {AuthButton, AuthHeader} from '../../../Componant';
import {styles} from './selectCategoryScreenStyles';
import CheckBox, {CheckBoxBase} from '@react-native-community/checkbox';
import {Rs, Color} from '../../../Helper';
import {} from 'react-native';

export default class selectCategoryScreen extends PureComponent {
  render() {
    return (
      <ScrollView>
        <View>
          <AuthHeader isBack {...this.props} />
          <Text style={styles.mainText}>
            Let us know how we {'\n'} can help you
          </Text>
          <Text style={styles.subText}>You can always change this later</Text>
          <View style={styles.center}>
            <View style={styles.cardView}>
              <Text style={styles.text}>Wight Loss</Text>
              <CheckBox style={styles.checkbox} />
            </View>
            <View style={styles.cardView}>
              <Text style={styles.text}>Batter Sleeping Habbit</Text>
              <CheckBox
                tintColor={Color.themePurple}
                onCheckColor={Color.themePurple}
                onValueChange={value => console.log('selected', value)}
                boxType="circle"
              />
            </View>
            <View style={styles.cardView}>
              <Text style={styles.text}>Track My Nutrition</Text>
              <CheckBox />
            </View>
            <View style={styles.cardView}>
              <Text style={styles.text}>Improve Overall Fitness</Text>
              <CheckBox />
            </View>
          </View>
          <AuthButton
            text="Continue"
            screen="CustomizeinterestScreen"
            {...this.props}
          />
        </View>
      </ScrollView>
    );
  }
}
