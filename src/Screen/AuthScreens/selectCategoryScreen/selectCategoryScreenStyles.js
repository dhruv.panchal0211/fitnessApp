import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  mainText: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 20,
  },
  subText: {
    fontSize: 15,
    color: Color.gray,
    textAlign: 'center',
    lineHeight: 40,
  },
  cardView: {
    flexDirection: 'row',
    width: Rs.widthPx(80),
    height: Rs.heightPx(10),
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Color.white,
    marginTop: 15,
    borderRadius: 15,
    alignContent: 'center',
  },
  center: {
    alignItems: 'center',
  },
  checkbox: {
    justifyContent: 'flex-end',
  },
  text: {
    marginLeft: 20,
  },
});
