import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  whoIs: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 35,
  },
  maleFemale: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-evenly',
  },
  logo: {
    tintColor: Color.black,
    height: Rs.heightPx(17),
    width: Rs.widthPx(19),
    marginTop: 7,
  },
  selectedLogo: {
    tintColor: Color.themePurple,
    height: Rs.heightPx(17),
    width: Rs.widthPx(19),
    marginTop: 7,
  },
  logoStyle: {
    width: Rs.widthPx(25),
    height: Rs.heightPx(20),
    alignItems: 'center',
  },
  toGive: {
    color: Color.gray,
    textAlign: 'center',
    marginTop: 40,
  },
  btnColor: {
    backgroundColor: Color.themePurple,
    height: Rs.heightPx(5),
    width: Rs.widthPx(35),
    borderRadius: 30,
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  btnText: {
    textAlign: 'center',
    justifyContent: 'center',
    color: Color.white,
  },
});
