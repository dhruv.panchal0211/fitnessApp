import React, {PureComponent} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {AuthButton, AuthHeader} from '../../../Componant';
import {Images, Screen} from '../../../Helper';
import Const from '../../../Helper/const';
import Utility from '../../../Helper/Utility';
import {styles} from './maleFemaleScreenStyles';

class maleFemaleScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
      selected1: false,
      gender: '',
    };
  }
  onPressContinue = () => {
    if (this.state.gender === '') {
      Utility.showToast('Please Select At Least One Option');
      return;
    }
    console.log(this.state.name);
    const {name} = this.props;
    name.addUserGender(this.state.gender);
    this.props.navigation.navigate(Screen.ProfilePictureScreen);
  };
  render() {
    const {selected, selected1} = this.state;
    return (
      <View>
        <AuthHeader isBack {...this.props} />
        <Text style={styles.whoIs}>Which One Are You?</Text>
        <View style={styles.maleFemale}>
          <TouchableOpacity
            onPress={() => {
              this.setState({selected: !selected, gender: 'male'});
            }}>
            <View style={styles.logoStyle}>
              <Image
                source={Images.male}
                style={this.state.selected ? styles.selectedLogo : styles.logo}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.setState({selected1: !selected1, gender: 'female'});
            }}>
            <View style={styles.logoStyle}>
              <Image
                source={Images.female}
                style={this.state.selected1 ? styles.selectedLogo : styles.logo}
              />
            </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.toGive}>
          To give you a batter experience {'\n'} we need to know your gender
        </Text>
        <TouchableOpacity
          onPress={() => {
            this.onPressContinue();
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  console.log(Const.authAction);
  return {
    name: bindActionCreators(Const.userAction, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(maleFemaleScreen);
