import React, {PureComponent} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {AuthButton, AuthHeader} from '../../../Componant';
import {Screen} from '../../../Helper';
import Const from '../../../Helper/const';
import Utility from '../../../Helper/Utility';
import {styles} from './SignupScreenStyles';
import messaging from '@react-native-firebase/messaging';

class SignupScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      cpassword: '',
    };
  }
  async getToken() {
    const {fcm} = this.props;
    console.log('ffccmm:', fcm);
    console.log('get token called');
    let fcmToken;
    console.log('get fcmToken Called', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('check fcm token', fcmToken);
        global.fcmToken = fcmToken;
        fcm.addFcm(fcmToken);
      }
    }
  }
  onPressSignup = () => {
    const {email, password, cpassword} = this.state;
    const {auth} = this.props;
    console.log('email:', email, 'password:', password.length);
    if (!email.trim()) {
      Utility.showToast('Enter Email');
      return;
    }
    if (!password.trim()) {
      Utility.showToast('Enter Password');
      return;
    }
    if (!cpassword.trim()) {
      Utility.showToast('Enter Confirm Password');
      return;
    }
    if (password.length < 6) {
      Utility.showToast('PLease Enter Min 6 Digit Password');
      return;
    }
    if (password != cpassword) {
      Utility.showToast('Password doent match');
      return;
    }
    auth
      .signup(email, password)
      .then(async resolve => {
        // await this.getToken();
        await this.props.navigation.navigate(Screen.NameScreen);
        this.setState({email: '', password: ''});
      })
      .catch(error => {
        Utility.showToast(error);
      });
  };
  render() {
    return (
      <View>
        <AuthHeader isBack {...this.props} />
        <Text style={styles.whatIsEmail}>What Is Your E-Mail Address?</Text>
        <View style={styles.textField}>
          <TextInput
            placeholder="Enter Your Email ID Here"
            keyboardType="email-address"
            style={styles.txtFld}
            autoFocus
            placeholderTextColor="#333"
            onChangeText={email => {
              this.setState({email});
            }}
          />
        </View>
        <View style={styles.textField}>
          <TextInput
            placeholder="Enter Your Password Here"
            style={styles.txtFld}
            secureTextEntry
            placeholderTextColor="#333"
            onChangeText={password => {
              this.setState({password});
            }}
          />
        </View>
        <View style={styles.textField}>
          <TextInput
            placeholder="Confirm Password"
            style={styles.txtFld}
            secureTextEntry
            placeholderTextColor="#333"
            onChangeText={cpassword => {
              this.setState({cpassword});
            }}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            this.onPressSignup();
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  console.log(Const.authAction);
  return {
    auth: bindActionCreators(Const.authAction, dispatch),
    fcm: bindActionCreators(Const.authAction, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(SignupScreen);
