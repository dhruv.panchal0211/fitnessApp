import React, {PureComponent} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {AuthButton, AuthHeader} from '../../../Componant';
import {Screen} from '../../../Helper';
import Const from '../../../Helper/const';
import Utility from '../../../Helper/Utility';
import {styles} from './NameScreenStyles';
import messaging from '@react-native-firebase/messaging';

class NameScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
  }
  onPressContinue = () => {
    if (this.state.name === '') {
      Utility.showToast('Please Enter Name');
      return;
    }
    console.log(this.state.name);
    const {name} = this.props;
    name.addUserName(this.state.name);
    this.props.navigation.navigate(Screen.MaleFemaleScreen);
  };

  render() {
    return (
      <View>
        <AuthHeader isBack {...this.props} />
        <Text style={styles.whatIsEmail}>What Is Your Name?</Text>
        <View style={styles.textField}>
          <TextInput
            placeholder="Enter Your Name"
            style={styles.txtFld}
            autoFocus
            placeholderTextColor="#333"
            onChangeText={name => {
              this.setState({name});
            }}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            this.onPressContinue();
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  console.log(Const.authAction);
  return {
    name: bindActionCreators(Const.userAction, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(NameScreen);
