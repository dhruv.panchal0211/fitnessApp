import React, {PureComponent} from 'react';
import {Text, View, FlatList, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AuthHeader, AuthButton} from '../../../Componant';
import {Images} from '../../../Helper';
import {styles} from './ProfilePictureScreenStyles';
import * as ImagePicker from 'react-native-image-picker';

export default class ProfilePictureScreenStyles extends PureComponent {
  constructor() {
    super();
    this.state = {
      Data: [
        {
          name: 'Male 1',
          photo: Images.male1,
        },
        {
          name: 'Male 2',
          photo: Images.male2,
        },
        {
          name: 'Male 3',
          photo: Images.male3,
        },
        {
          name: 'Male 4',
          photo: Images.male4,
        },
        {
          name: 'Male 5',
          photo: Images.male5,
        },
        {
          name: 'Female 1',
          photo: Images.female1,
        },
        {
          name: 'Female 2',
          photo: Images.female2,
        },
        {
          name: 'Female 3',
          photo: Images.female3,
        },
        {
          name: 'Female 4',
          photo: Images.female4,
        },
        {
          name: 'Female 5',
          photo: Images.female5,
        },
      ],
      image: '',
    };
  }

  profileRender = itemData => {
    console.log('reder', itemData);
    return (
      <View style={styles.container}>
        <Image source={itemData.item.photo} style={styles.profile} />
      </View>
    );
  };

  onPressAdd = () => {
    const options = {
      mediaType: 'photo',
      quality: 1,
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log(response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          image: {
            name: new Date().getMilliseconds().toString(),
            type: response.type,
            uri: 'file://' + response.path,
          },
        });
      }
    });
  };
  render() {
    return (
      <View>
        <AuthHeader isBack {...this.props} />

        <View style={styles.purpleRing} />
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.Data}
          keyExtractor={(item, index) => index}
          renderItem={this.profileRender}
        />

        <Text style={styles.selectProfile}>
          You can select the photo from one of {'\n'} this emoji or add your own
          photo {'\n'}
          as your profile picture
        </Text>
        <TouchableOpacity onPress={this.onPressAdd}>
          <Text style={styles.addProfile}>Add Custom Photo</Text>
        </TouchableOpacity>

        <AuthButton
          text="Continue"
          screen="SelectCategoryScreen"
          {...this.props}
        />
      </View>
    );
  }
}
