import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  container: {
    width: Rs.widthPx(100),
    alignItems: 'center',
    marginTop: 50,
  },
  profile: {
    height: Rs.heightPx(15),
    width: Rs.heightPx(15),
    borderRadius: 50,
    resizeMode: 'cover',
    borderRightWidth: 9,
    borderColor: Color.themePurple,
  },
  selectProfile: {
    fontSize: 16,
    color: Color.gray,
    textAlign: 'center',
    marginTop: 30,
  },
  addProfile: {
    fontSize: 16,
    color: Color.themePurple,
    textAlign: 'center',
    marginTop: 10,
  },
  purpleRing: {
    borderColor: Color.themePurple,
    height: Rs.heightPx(17),
    width: Rs.heightPx(17),
    borderWidth: 2,
    borderRadius: 60,
    marginBottom: -170,
    marginLeft: 135,
    overflow: 'hidden',
  },
});
