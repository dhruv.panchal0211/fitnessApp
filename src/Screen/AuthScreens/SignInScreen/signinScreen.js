import React, {PureComponent} from 'react';
import {Image, View, TextInput, TouchableOpacity, Text} from 'react-native';
import {Images, Screen} from '../../../Helper';
import {styles} from './signinScreenStyles';
import {AuthButton, AuthHeader} from '../../../Componant';
import Utility from '../../../Helper/Utility';
import {bindActionCreators} from 'redux';
import Const from '../../../Helper/const';
import {connect} from 'react-redux';
import messaging from '@react-native-firebase/messaging';

class signinScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      userInfo: {},
    };
  }

  async getToken() {
    const {fcm} = this.props;
    console.log('ffccmm:', fcm);
    console.log('get token called');
    let fcmToken;
    console.log('get fcmToken Called', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('check fcm token', fcmToken);
        global.fcmToken = fcmToken;
        fcm.addFcm(fcmToken);
      }
    }
  }

  onPressLogin = async () => {
    const {auth} = this.props;
    console.log('loginauth:', auth);
    const {email, password} = this.state;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    console.log('email:', email, 'password:', password.length);
    if (reg.test(!email) === true) {
      Utility.showToast('Enter Vaild Email');
      return;
    }
    if (!email.trim()) {
      Utility.showToast('Enter Email');
      return;
    }
    if (!password.trim()) {
      Utility.showToast('Enter Password');
      return;
    }
    if (password.length < 6) {
      Utility.showToast('Please Enter Min 6 Digit Password');
      return;
    }
    console.log('saassa');

    auth
      .login(email, password)
      .then(async resolve => {
        await this.getToken();
        await this.props.navigation.navigate(Screen.SideScreen);
        this.setState({email: '', password: ''});
      })
      .catch(error => {
        Utility.showToast(error);
      });
  };
  render() {
    return (
      <View>
        <AuthHeader isBack {...this.props} />
        <View style={styles.textField}>
          <Image source={Images.person} style={styles.perPass} />
          <TextInput
            placeholder="Enter Your Email ID Here"
            style={styles.txtFld}
            placeholderTextColor="#333"
            onChangeText={email => {
              this.setState({email});
            }}
          />
        </View>
        <View style={styles.textField}>
          <Image source={Images.pass} style={styles.perPass} />
          <TextInput
            placeholder="Enter Your Password Here"
            style={styles.txtFld}
            placeholderTextColor="#333"
            secureTextEntry
            onChangeText={password => {
              this.setState({password});
            }}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            this.onPressLogin();
            // this.props.navigation.navigate(Screen.SideScreen);
          }}>
          <View style={styles.btnColor}>
            <Text style={styles.btnText}>Continue</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    auth: bindActionCreators(Const.authAction, dispatch),
    fcm: bindActionCreators(Const.authAction, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(signinScreen);
