import React, {PureComponent} from 'react';
import {
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Images} from '../../../Helper';
import {styles} from './welcomeFinalScreenStyles';

export default class welcomeFinalScreen extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={Images.background} style={styles.background}>
          <View style={styles.logoView}>
            <View style={styles.logoWhiteView}>
              <Image source={Images.logo} style={styles.logo} />
            </View>
            <Text style={styles.readyToGo}>You are ready to go!</Text>
            <Text style={styles.thanksFont}>
              Thanks for taking your time to create {'\n'} account with us.this
              is the fun {'\n'} part,let's explore the app.
            </Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('SideScreen')}>
              <Image source={Images.next} style={styles.nextLogo} />
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
