import {StyleSheet} from 'react-native';
import {Rs, Color} from '../../../Helper';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
  },
  logo: {
    width: Rs.heightPx(15),
    height: Rs.heightPx(15),
  },
  logoView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  logoWhiteView: {
    width: Rs.heightPx(15),
    height: Rs.heightPx(15),
    backgroundColor: Color.white,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 90,
    overflow: 'hidden',
    top: -50,
  },
  readyToGo: {
    color: Color.white,
    fontSize: 22,
    fontWeight: 'bold',
  },
  thanksFont: {
    textAlign: 'center',
    color: Color.white,
    lineHeight: 20,
  },
  nextLogo: {
    tintColor: Color.white,
    height: Rs.heightPx(7),
    width: Rs.heightPx(7),
    marginTop: 20,
  },
});
