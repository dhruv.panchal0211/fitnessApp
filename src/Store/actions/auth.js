import {Alert} from 'react-native';
import Utility from '../../Helper/Utility';
import Storage from '../../Helper/Storage';

export const SIGNUP = 'SIGNUP';
export const LOGIN = 'LOGIN';
export const ADD_FCM = 'ADD_FCM';

export const signup = (email, password) => {
  return async dispatch => {
    return new Promise(async (resolve, reject) => {
      const response = await fetch(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyC01F23L_TlceBW70T9WsgZbuULQcnhL8Q',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: email,
            password: password,
            returnSecureToken: true,
          }),
        },
      );
      console.log('Sugnup Log', response);
      const resData = await response.json();
      if (!response.ok) {
        console.log('Signup resData:', resData.error.message);
      }

      if (!response.ok) {
        if (resData.error.message === 'EMAIL_EXISTS') {
          Utility.showToast('EMAIL_EXISTS');
          console.log('treeeeeee');
          reject('EMAIL_EXISTS');
        }

        if (resData.error.message === 'OPERATION_NOT_ALLOWED') {
          Utility.showToast('OPERATION_NOT_ALLOWED');
          reject('OPERATION_NOT_ALLOWED');
        }

        if (resData.error.message === 'INVALID_EMAIL') {
          Utility.showToast('INVALID_EMAIL');
          reject('INVALID_EMAIL');
        }

        if (resData.error.message === 'TOO_MANY_ATTEMPTS_TRY_LATER') {
          Utility.showToast('TOO_MANY_ATTEMPTS_TRY_LATER');
          reject('TOO_MANY_ATTEMPTS_TRY_LATER');
        }
        return;
      }

      if (response.status === 200) {
        const userData = {
          token: resData.idToken,
          userId: resData.localId,
        };
        resolve();
        dispatch({
          type: SIGNUP,
          token: resData.idToken,
          userId: resData.localId,
        });
        Storage.setUserData(userData);
      }
    });
  };
};

export const login = (email, password) => {
  return async dispatch => {
    return new Promise(async (resolve, reject) => {
      console.log('asasasa');
      const response = await fetch(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC01F23L_TlceBW70T9WsgZbuULQcnhL8Q',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: email,
            password: password,
            returnSecureToken: true,
          }),
        },
      );
      console.log(response);
      const resData = await response.json();
      if (!response.ok) {
        console.log('errrrooorrrr');
        console.log('Login resData:', resData.error.message);

        if (resData.error.message === 'EMAIL_NOT_FOUND') {
          Utility.showToast('EMAIL_NOT_FOUND');
          reject('EMAIL_NOT_FOUND');
        }

        if (resData.error.message === 'INVALID_EMAIL') {
          Utility.showToast('INVALID_EMAIL');
          reject('INVALID_EMAIL');
        }

        if (resData.error.message === 'INVALID_PASSWORD') {
          Utility.showToast('INVALID_PASSWORD');
          reject('INVALID_PASSWORD');
        }

        if (resData.error.message === 'USER_DISABLED') {
          Utility.showToast('USER_DISABLED');
          reject('USER_DISABLED');
        }
        return;
      }
      if (response.status === 200) {
        const userData = {
          token: resData.idToken,
          userId: resData.localId,
        };
        resolve();
        dispatch({
          type: LOGIN,
          token: resData.idToken,
          userId: resData.localId,
        });
        Storage.setUserData(userData);
      }
    });
  };
};

export const addFcm = fcmToken => {
  return async dispatch => {
    const responce = await fetch(
      'https://rn-fitnes-default-rtdb.firebaseio.com//fcm.json',
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
          fcmToken,
        }),
      },
    );
    const resData = await responce.json();
    console.log('fcmData', resData);
    dispatch({
      type: ADD_FCM,
      fcmToken: {
        fcmToken,
      },
    });
  };
};
