export const USER_NAME = 'USER_NAME';
export const USER_GENDER = 'USER_GENDER';

export const addUserName = name => {
  return async dispatch => {
    const ownerId = global.userData.userId;
    try {
      const responce = await fetch(
        `https://rn-fitnes-default-rtdb.firebaseio.com/Users/${ownerId}.json`,
        {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify({
            ownerId,
            name,
          }),
        },
      );
      const resData = await responce.json();
      console.log('resData', resData);
      dispatch({
        type: USER_NAME,
        productData: {
          id: resData.name,
          name,
        },
      });
    } catch (err) {
      throw err;
    }
  };
};

export const addUserGender = gender => {
  return async dispatch => {
    const ownerId = global.userData.userId;
    try {
      const responce = await fetch(
        `https://rn-fitnes-default-rtdb.firebaseio.com/Users/${ownerId}.json`,
        {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
          },
          body: JSON.stringify({
            gender,
          }),
        },
      );
      const resData = await responce.json();
      console.log('resData', resData);
      dispatch({
        type: USER_GENDER,
        productData: {
          id: resData.name,
          gender,
        },
      });
    } catch (err) {
      throw err;
    }
  };
};
