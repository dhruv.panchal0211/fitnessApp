import {USER_NAME, USER_GENDER} from '../actions/user';

const initialState = {
  name: '',
  gender: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_NAME:
      return {
        name: action.name,
        gender: action.gender,
      };
    case USER_GENDER:
      return {
        gender: action.gender,
      };
  }

  return state;
};
