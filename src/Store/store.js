import {combineReducers, createStore, applyMiddleware} from 'redux';
import auth from './reducers/auth';
import user from './reducers/user';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';

const AppReducers = combineReducers({
  auth: auth,
  user: user,
});

const rootReducer = (state, action) => {
  return AppReducers(state, action);
};

const store = createStore(rootReducer, applyMiddleware(ReduxThunk, logger));

export default store;
