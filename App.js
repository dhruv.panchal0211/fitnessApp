import React, {PureComponent} from 'react';
import Router from './src/Router/Router';
import {RootSiblingParent} from 'react-native-root-siblings';
import {Provider} from 'react-redux';
import store from './src/Store/store';
import Storage from './src/Helper/Storage';
import {ImageBackground, Text, View} from 'react-native';
import {Images} from './src/Helper';

export default class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  async componentDidMount() {
    await Storage.getUserData();
    setTimeout(async () => {
      await this.setState({loading: false});
    }, 3000);
  }
  render() {
    const {loading} = this.state;
    return (
      <View style={{flex: 1}}>
        {!loading ? (
          <Provider store={store}>
            <RootSiblingParent>
              <Router />
            </RootSiblingParent>
          </Provider>
        ) : (
          <View>
            <ImageBackground
              source={Images.splash}
              style={{width: '100%', height: '100%'}}
            />
          </View>
        )}
      </View>
    );
  }
}
